import express from 'express';
import { createServer } from 'http';
import socketIo from 'socket.io';
import * as path from 'path';
import { v4 } from 'uuid';
import { GameState } from '../src/store/gameState';

// interface GameState {
//   gameSessionId: string;
//   gameStatus: 'START' | 'CALLIBRATING' | 'INPROGRESS' | 'DONE' | 'BAD';
// }

type GameRoom = Record<string, GameState>;

const port = Number(process.env.PORT || 8080);
const app = express();
const server = createServer(app);
const io = socketIo(server);

const gameRooms: GameRoom = {};

interface Request {
  uniqueId: string;
}

io.on('connection', function(socket: socketIo.Socket){
  socket.on('START', function (msg: Request) {
    const uniqueId = 'A';
    socket.join(uniqueId);
    gameRooms[uniqueId] = {
      gameSessionId: uniqueId,
      gameStatus: 'CONNECTION',
    }
    socket.join(uniqueId);
    socket.emit('GAME_SYNC', gameRooms[uniqueId]);
  });
  socket.on('JOIN', (msg: any) => {
    const { uniqueId } = msg;
    const computedUniqueId = Object.keys(gameRooms).find(key => key.includes(uniqueId));
    if (computedUniqueId in gameRooms && gameRooms[computedUniqueId].gameStatus === 'CONNECTION') {
      gameRooms[computedUniqueId] = {
        gameSessionId: computedUniqueId,
        gameStatus: 'CALLIBRATING',
      }
      socket.join(computedUniqueId);
      io.in(computedUniqueId).emit('GAME_SYNC', gameRooms[computedUniqueId]);
    }
  });
  socket.on('ORIENTATION', (msg: any) => {
    const { uniqueId, orientationData } = msg;
    gameRooms[uniqueId] = {
      ...gameRooms[uniqueId],
      orientation: orientationData,
    }
    socket.to(uniqueId).emit('GAME_SYNC', gameRooms[uniqueId]);
  });
  socket.on('ACCELERATION', (msg: any) => {
    const { uniqueId, accelerationData } = msg;
    gameRooms[uniqueId] = {
      ...gameRooms[uniqueId],
      acceleration: accelerationData,
    }
    socket.to(uniqueId).emit('GAME_SYNC', gameRooms[uniqueId]);
  });
});

app.use(express.static(path.join(__dirname, '../build')));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../build', 'index.html'));
});

server.listen(port, () => console.log(`Example app listening on port ${port}!`));
