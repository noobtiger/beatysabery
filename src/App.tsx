import React from 'react';
import { Route } from './components';
import { StoreProvider } from 'easy-peasy';
import { store } from './store/store';

const App: React.FC = () => {
  return (
    <StoreProvider store={store}>
      <Route />
    </StoreProvider>
  );
}

export default App;
