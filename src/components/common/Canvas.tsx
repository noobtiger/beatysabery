import React from 'react';
import { Canvas as ThreeCanvas } from 'react-three-fiber';

const Canvas: React.FC = (props) => {
  const { children } = props;
  return (
    <ThreeCanvas style={{height: '100%'}}>
      {children}
    </ThreeCanvas>
  );
}

export { Canvas };
