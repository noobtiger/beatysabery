import React from 'react';
import { Game, Menu, Connect, Mobile, Calibrate, GameScreen } from '../../components';
import { useStoreState, useStoreActions } from '../../store/store';
import { setupSocketConnection } from '../../store/action';

export const Route = () => {
  const gameState = useStoreState(state => state.game.gameState); 
  const gameStatus = gameState && gameState.gameStatus ? gameState.gameStatus : 'MENU';
  const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  const updateGameState = useStoreActions(state => state.game.updateGameState);
  const handleClick = () => {
    setupSocketConnection(updateGameState);
  } 
  return (
    <div style={{height: '100%',  backgroundColor: 'black'}}>
      {gameStatus === 'MENU' && !isMobile ? <Menu handleClick={handleClick}/> : null}
      {gameStatus === 'CONNECTION' && !isMobile ? <Connect /> : null}
      {gameStatus === 'CALLIBRATING' && !isMobile ? <Calibrate /> : null}
      {gameStatus === 'INPROGRESS' && !isMobile ? <Game /> : null}
      {isMobile && ['MENU', 'CONNECTION'].includes(gameStatus) ? <Mobile /> : null}
      {isMobile && ['CALLIBRATING', 'INPROGRESS'].includes(gameStatus) ? <GameScreen /> : null}
    </div>
  );
}
