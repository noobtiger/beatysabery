import React from 'react';
import { Canvas } from '../';

const Game: React.FC = (props) => {
  const { children } = props;
  return (
    <Canvas>
      {children}
    </Canvas>
  );
}

export {Game};
