import React from 'react';
import { useFrame, useThree } from 'react-three-fiber';
import * as THREE from 'three';

interface Props {
  initialX: number;
  initalY: number;
  initialZ: number;
}
export const Block: React.FC<Props> = (props) => {
  const { initalY, initialX, initialZ } = props;
  const ref = React.useRef();
  const [ positionZ, updatePositionZ ] = React.useState(initialZ);
  const { scene } = useThree();
  useFrame(() => {
    if(ref && ref.current) {
      updatePositionZ(positionZ+0.1);
      // (ref.current as any).position.z += 0.1;
      (ref.current as any).rotation.x += 0.01;
      (ref.current as any).rotation.y += 0.01;
      (ref.current as any).rotation.z += 0.01;
      if(positionZ > 20) {
        updatePositionZ(initialZ);
      }
      // const player = scene.getObjectByName('player');
      // if(player) {
      //   const ray = new THREE.Raycaster();
      //   const collisionResults = ray.intersectObjects((ref.current as any).geometry);
      //   console.log(collisionResults);
      //   const originPoint = player.position.clone();
      //   //@ts-ignore
      //   for (let vertexIndex = 0; vertexIndex < player.geometry.vertices.length; vertexIndex++) {
      //           //@ts-ignore
      //     const localVertex = player.geometry.vertices[vertexIndex].clone();
      //     const globalVertex = localVertex.applyMatrix4(player.matrix);
      //     const directionVector = globalVertex.sub(player.position);
      //     const ray = new THREE.Raycaster(originPoint, directionVector.clone().normalize());
      //     const collisionResults = ray.intersectObjects((ref.current as any).geometry);
      //     if (collisionResults.length > 0 && collisionResults[0].distance < directionVector.length()) {
      //         console.log(collisionResults);
      //     }
      //   }
      //}
    }
  });
  const position = [initialX,initalY, positionZ];
  return (
    <mesh ref={ref} position={position}>
      <boxGeometry attach="geometry" />
      <meshBasicMaterial attach="material" color="blue" />
    </mesh>
  );
}