import React from 'react';
import { Block } from '../';

interface Props {

}

const blocks = [
  {
    x: 4,
    y: -2,
    z: -40,
  },
  {
    x: 4,
    y: 2,
    z: -50,
  },
  {
    x: -4,
    y: 2,
    z: -80,
  },
  {
    x: -4,
    y: -2,
    z: -120,
  },
  {
    x: -4,
    y: -4,
    z: -150,
  },
  {
    x: 4,
    y: 3,
    z: -150,
  },
];

export const Blocks: React.FC<Props> = (props) => {
  const blocksContainer = blocks.map((block, index) => {
    const { x,y, z } = block;
    return (
      <Block key={index} initialX={x} initalY={y} initialZ={z} />
    );
  });
  return (
    <group>
      {blocksContainer}
    </group>
  )
}