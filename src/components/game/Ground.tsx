import React from 'react';

export const Ground: React.FC = () => {
  return (
    <mesh position={[0,-3,0]}>
      <planeGeometry attach="geometry" />
      <meshBasicMaterial attach="material" color="darkgray" />
    </mesh>
  );
}