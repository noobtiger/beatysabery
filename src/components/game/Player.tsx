import React from 'react';
import { Quaternion } from 'three';

interface Props {
  angle: Quaternion;
}

export const Player: React.FC<Props> = (props) => {
  const { angle } = props;
  return (
    <group quaternion={angle} position={[0,-3,0]}>
    <mesh position={[0,-3,0]}>
      <mesh position={[0,3,0]}>
         <boxGeometry attach="geometry" args={[0.2,0.6, 0.2]} />
         <meshBasicMaterial attach="material" color="purple" />
      </mesh>
       <mesh name="player">
         <cylinderGeometry attach="geometry" args={[0.1, 0.1, 6]}/>
         <meshBasicMaterial attach="material" color="blue" />
       </mesh>
     </mesh>
   </group>
  );
}