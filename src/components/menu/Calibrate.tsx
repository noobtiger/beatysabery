import React from 'react';
import { Game } from '../common';
import { Player, Blocks } from '../';
import { useStoreState } from '../../store/store';
import * as THREE from 'three';

interface Props {
}

export const Calibrate: React.FC<Props> = (props) => {
  const calibrateStepStyle: React.CSSProperties = {
    color: 'white',
    fontSize: '1.2em',
    position: 'absolute',
    zIndex: 100,
    bottom: '0px',
    textAlign: 'center',
    marginBottom: '10px',
  };
  const gameState = useStoreState(state => state.game.gameState); 
  const orientation = gameState && gameState.orientation ? gameState.orientation : [0,0,0,0];
  const quaternion = new THREE.Quaternion();
  const angle = quaternion.fromArray(orientation);
  return (
    <div style={{height: '100%'}}>
      <Game>
        <Player angle={angle}/>
        <Blocks />
      </Game>
      <div style={calibrateStepStyle}>
        
      </div>
    </div>
  );
}