import React from 'react';
import { useStoreState } from '../../store/store';

interface Props {
}

const Connect: React.FC<Props> = (props) => {
  const helpTextContainerStyle: React.CSSProperties = {
    fontSize: '1.2em',
    color: 'white',
    padding: '10px',
  };
  const uniqueIdStyle: React.CSSProperties = {
    fontSize: '1.4em',
    color: 'yellow',
  };
  const gameState = useStoreState(state => state.game.gameState); 
  const uniqueId = gameState && gameState.gameSessionId ? gameState.gameSessionId : '';
  const lastDashIndex = uniqueId.lastIndexOf('-');
  const smallerId = uniqueId.slice(lastDashIndex+1, uniqueId.length);
  return (
    <main>
      <section style={helpTextContainerStyle}>
        Go to this website on your smartphone and enter this code 
        <span style={uniqueIdStyle}> {smallerId} </span>
        and start game.
      </section>
    </main>
  );
};

export { Connect };