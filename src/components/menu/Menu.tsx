import React from 'react';

interface Props {
  handleClick: () => void;
}

const Menu: React.FC<Props> = (props) => {
  const menuContainerStyle: React.CSSProperties = {
    width: '100%',
    textAlign: 'center',
    zIndex: 100,
    color: 'white',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
  };
  const titleStyle: React.CSSProperties = {
    fontSize: '4em',
    display: 'flex',
    flexDirection: 'column',
    fontWeight: 600,
  };
  const beatyStyle: React.CSSProperties = {
    color: 'red',
  };
  const saberyStyle: React.CSSProperties = {
    color: 'blue',
  };
  const startButtonStyle: React.CSSProperties = {
    padding: '20px',
    backgroundColor: 'green',
    width: '200px',
    borderRadius: '2em',
  };
  const buttonContainerStyle: React.CSSProperties = {
    display: 'flex',
    justifyContent: 'center',
    cursor: 'pointer',
  };
  return (
    <div style={menuContainerStyle}>
      <section style={titleStyle}>
        <div style={beatyStyle}>
          Phone
        </div>
        <div style={saberyStyle}>
          Saber
        </div>
      </section>
      <section style={buttonContainerStyle}>
        <div onClick={props.handleClick} style={startButtonStyle}>
          Start Game
        </div>
      </section>
    </div>
  );
}

export {Menu};
