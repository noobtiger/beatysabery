import React from 'react';
import { useStoreState } from '../../store/store';
import { sendOrientationData, sendAccelerationData } from '../../store/action';

export const GameScreen: React.FC = () => {
  const rootContainerStyle: React.CSSProperties = {
    color: 'white',
    fontSize: '1.5em',
    flex: 1,
    backgroundColor: 'blue',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  };
  const scoreContainerStyle: React.CSSProperties = {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  };
  const buttonStyle: React.CSSProperties = {
    padding: '10px',
    margin: '0 10px',
    backgroundColor: 'white',
    textAlign: 'center',
    cursor: 'pointer',
    width: '100%',
    color: 'black',
  };
  const resetButtonStyle: React.CSSProperties = {
    border: '1px solid black',
    marginBottom: '10px',
  }

  React.useEffect(() => {
    //@ts-ignore
    const sensor = new AbsoluteOrientationSensor({frequency: 30});
    sensor.onreading = () => {
      updateOrientation(sensor.quaternion)
    };
    sensor.onerror = (event: any) => {
        if (event.error.name == 'NotReadableError') {
            console.log("Sensor is not available.");
        }
    }
    sensor.start();
  }, [])

  const gameState = useStoreState(state => state.game.gameState); 
  const gameStatus = gameState && gameState.gameStatus ? gameState.gameStatus : 'MENU';
  const uniqueId = gameState && gameState.gameSessionId ? gameState.gameSessionId : '';

  const updateOrientation = (data: any) => {
    sendOrientationData(uniqueId, data);
  }

  const updateAcceleration = (data: any) => {
    sendAccelerationData(uniqueId, data);
  }

  // const nextButton = gameStatus === 'CALLIBRATING' && (
  //   <div style={buttonStyle}>
  //     Press next to continue
  //   </div>
  // );
  // const resetButton = gameStatus === 'CALLIBRATING' && (
  //   <div style={{...buttonStyle, ...resetButtonStyle}}>
  //     Reset 
  //   </div>
  // );
  return (
    <div style={rootContainerStyle}>
      <div style={scoreContainerStyle}>
        Score: TBD
      </div>
    </div>
  )
}