import React from 'react';
import { setupSocketConnection } from '../../store/action';
import { useStoreActions } from '../../store/store';

export const Mobile: React.FC = (props) => {
  const [ uniqueId, updateUniqueId ] = React.useState('');
  const inputStyle: React.CSSProperties = {
    width: '100%',
    padding: '12px 20px',
    margin: '8px 0',
    display: 'inline-block',
    border: '1px solid #ccc',
    borderRadius: '4px',
    boxSizing: 'border-box',
    fontSize: '1.2em',
  };
  const connectButtonStyle: React.CSSProperties = {
    padding: '10px',
    margin: '0 10px',
    backgroundColor: 'white',
    textAlign: 'center',
    borderRadius: '2em',
    cursor: 'pointer',
    width: '200px',
    marginTop: '20px',
  };
  const containerStyle: React.CSSProperties = {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  };
  const updateGameState = useStoreActions(state => state.game.updateGameState);
  const handleStartClick = () => {
    setupSocketConnection(updateGameState, 'JOIN', uniqueId);
  }
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    updateUniqueId(event.target.value);
  }
  return (
    <div style={containerStyle}>
      <input
        style={inputStyle}
        type="text"
        placeholder="Enter unique Id"
        value={uniqueId}
        onChange={handleChange}
      />
      <div style={connectButtonStyle} onClick={handleStartClick}>
        Start game
      </div>
    </div>
  );
};
