import { socketIoClient } from './client';
import { GameState } from './gameState';

let socketClient: any = null;

export const setupSocketConnection = (
  cb: (gameState: GameState) => void,
  startEventName: string = 'START',
  uniqueId?: string,
)  => {
  try {
    socketClient = socketIoClient.connectSocket().getSocketClient();
    if (startEventName === 'START') {
      socketClient.emit(startEventName);
    } else {
      socketClient.emit(startEventName, { uniqueId });
    }

    socketClient.on('GAME_SYNC', (msg: GameState) => {
      cb(msg);
    });
  } catch(er) {
    console.log(er);
  }
};

export const sendOrientationData = (uniqueId: string, orientationData: any) => {
  socketClient.emit('ORIENTATION', { uniqueId, orientationData });
}

export const sendAccelerationData = (uniqueId: string, accelerationData: any) => {
  socketClient.emit('ACCELERATION', { uniqueId, accelerationData });
}