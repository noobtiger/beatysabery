import socketIo from 'socket.io-client';

class SocketIoClient  {
  private socketClient: any;
  private baseUrl: string = process.env.NODE_ENV === 'production' ? '' : 'http://localhost:8080';

  public connectSocket() {
    this.socketClient = socketIo.connect(this.baseUrl);
    return this;
  }
  public getSocketClient() {
    return this.socketClient;
  }
}

export const socketIoClient = new SocketIoClient();
