import { Action, action } from 'easy-peasy';

export type SampleRow = (string|number)[];
export type GameStatus = 'MENU' | 'CONNECTION' |'CALLIBRATING' |'INPROGRESS' | 'END';

export interface GameState {
  gameSessionId: string;
  gameStatus: GameStatus;
  orientation?: any;
  acceleration?: any;
};

export interface GameStateModel {
  gameState: GameState | undefined;
  updateGameState: Action<GameStateModel, GameState>;
}

export const gameState: GameStateModel =  {
  gameState: undefined,
  updateGameState: action((state, payload) => {
    state.gameState = payload;
  })
};
