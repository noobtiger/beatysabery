import { createStore, createTypedHooks } from 'easy-peasy';
import { gameState, GameStateModel } from './gameState';

export interface StoreModel {
  game: GameStateModel;
};

const storeModel: StoreModel = {
  game: gameState,
};

const { useStoreActions, useStoreState, useStoreDispatch } = createTypedHooks<StoreModel>();

export { useStoreActions, useStoreState, useStoreDispatch };

export const store = createStore(storeModel);
